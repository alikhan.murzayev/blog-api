package main

import (
	"flag"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"gitlab.com/golang-demos/blog-api/src/blog"
	"gitlab.com/golang-demos/blog-api/src/config"
	"gitlab.com/golang-demos/blog-api/src/repository/scylladb"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

// Init vars for services
var (
	blogService blog.Service
)

//////////////////////////////////////
// Main blog-api function
//////////////////////////////////////
func main() {

	// Set http port
	httpAddr := flag.String("http.addr", ":8080", "HTTP listen address only port :8080")
	flag.Parse()

	// Init logger
	var logger log.Logger
	logger = log.NewLogfmtLogger(os.Stderr)
	logger = level.NewFilter(logger, level.AllowAll())
	logger = &serializedLogger{Logger: logger}
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)

	logr := &logrus.Logger{
		Out:   os.Stdout,
		Level: logrus.Level(5),
		Formatter: &logrus.TextFormatter{
			FullTimestamp: true,
		},
	}
	blog.Loger = logr

	// Get configs
	err := config.GetConfigs()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	// Init connections
	scyllaDBConnection, err := scylladb.ScyllaDBConnectionStart()
	if err != nil {
		panic(fmt.Errorf("ScyllaDB connection error: %s \n", err))
	}
	defer scyllaDBConnection.Close()

	blogCommandRepo := scylladb.NewPostCommandRepo(scyllaDBConnection)

	fieldKeys := []string{"method"}

	// Init blog service
	blogService = blog.NewService(
		blogCommandRepo,
	)
	blogService = blog.NewLoggingService(log.With(logger, "component", "blog"), blogService)
	blogService = blog.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "blog_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "blog_service",
			Name:      "request_latency_microseconds",
			Help:      "Total duration of requests in microseconds.",
		}, fieldKeys),
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "blog_service",
			Name:      "error_count",
			Help:      "Number of error requests received.",
		}, fieldKeys),
		blogService,
	)

	// Init http routes
	httpLogger := log.With(logger, "component", "http")

	mux := http.NewServeMux()
	mux.Handle("/blog-api/", blog.MakeHandler(blogService, httpLogger))

	http.Handle("/blog-api/", accessControl(mux))
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/check", healthChecks)

	errs := make(chan error, 2)

	// Init http serve
	go func() {
		_ = logger.Log("transport", "http", "address", *httpAddr, "msg", "listening blog-api")
		errs <- http.ListenAndServe(*httpAddr, nil)
	}()

	// Listen errors chan
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	_ = logger.Log("terminated", <-errs)
}

//////////////////////////////////////
// Additional structures & functions
//////////////////////////////////////
type serializedLogger struct {
	mtx sync.Mutex
	log.Logger
}

func (l *serializedLogger) Log(keyvals ...interface{}) error {
	l.mtx.Lock()
	defer l.mtx.Unlock()
	return l.Logger.Log(keyvals...)
}

func healthChecks(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "ok")
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization,X-Owner,darvis-dialog-id")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
